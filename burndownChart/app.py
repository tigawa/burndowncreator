import json
import requests
import pandas as pd
from pandas import json_normalize
import urllib.parse
import numpy as np
import matplotlib.pyplot as plt
from pytz import timezone
from datetime import datetime, timedelta
from matplotlib.offsetbox import TextArea, AnnotationBbox
import textwrap
import io
from concurrent.futures import ThreadPoolExecutor
from decimal import Decimal, ROUND_HALF_UP, ROUND_HALF_EVEN
import japanize_matplotlib  # 日本語化matplotlib
import math
import matplotlib.dates as mdates
import matplotlib.ticker as ticker
from matplotlib.dates import date2num
from matplotlib import gridspec
from matplotlib.ticker import MaxNLocator

# 定数
# PGできた時点で、完了にする割合 50%
MAKED_WEGIHT = 0.5
# 表示する日付の範囲　マイルストーンの期日の倍数 ※必ず小数点を含める  例) 2.0
DISP_RANGE_ENLARGEMENT = 2.0
# 予測値の平均日数
MEAN_RANGE_DAYS = 10
# マイルストーンの締切日が入っていない場合のプロジェクトの期間
DEFAULT_DAYS = 100


def lambda_handler(event, context):
    body = event["Records"][0]["body"]
    if isinstance(body, str):  # SQS用
        body_json = json.loads(body)
    else:  # テスト用
        body_json = body
    print(input, body_json)

    # ■必須■
    # プロジェクトID
    project_id = body_json["project-id"]
    # マイルストーン URLエンコード
    milestones = body_json['milestones']
    # URLエンコード
    milestones_url_encode = ([urllib.parse.quote(x) for x in milestones])

    # APIキーの指定
    apikey = body_json['gitlab-apikey']
    # slack channel
    slack_token = body_json['slack-token']
    # slack token
    slack_channel = body_json['slack-channel']

    # ■オプション■
    option = body_json["option"]
    # ラベル
    maked_labels = option.get("maked-labels", ['review'])
    # 除外issue [57, 54, 41]
    ignore_iids = option.get("ignore-iids", [])
    # review依頼時点でdoneにする割合 50%
    maked_wegiht = option.get("maked-wegiht", MAKED_WEGIHT)
    # 表示する日付の範囲　マイルストーンの期日の倍数 ※必ず小数点を含める  例) 2.0
    disp_range_enlargement = option.get(
        "disp-range-enlargement", DISP_RANGE_ENLARGEMENT)
    # 予測値を計算するためのの平均日数
    mean_range_days = option.get("mean-range-days", MEAN_RANGE_DAYS)
    # プロジェクト休暇　■スプレットシートを修正
    project_holidays = option.get("project_holidays", [])
    # 1日の計画マンパワー(人日)　■スプレットシートを修正
    base_man_power_rate = option.get("base_man_power_rate", 0)
    # バッファの割合 推奨=0.2 ■スプレットシートを修正
    buffer_rate = option.get("buffer_rate", 0)
    # マンパワーの調整
    man_powers_adjustment = option.get("man_powers_adjustment", [])
    # 休日も働くかどうか ■スプレットシートを修正
    is_working_holiday = option.get("is_working_holiday", False)
    # マンパワーとマイルストーンの期日の入力がある場合に、マイルストーンを優先するかどうか ■スプレットシートを修正
    is_milestone_first = option.get("is_milestone_first", False)
    # Burndownチャートに指定した日付を縦線(破線)として描画します。
    limit_days = option.get("limit_days", [])
    # １日の予測完了ポイントを指定します。（予測線に使用するもの）
    prediction_one_day_points = option.get("prediction_one_day_points", None)
    # -----------------------------------------------------------------
    d_today = pd.to_datetime('today').floor('D')
    headers = {'PRIVATE-TOKEN': apikey}
    tpool = ThreadPoolExecutor(max_workers=20)

    # 各種データ取得
    project_df = tpool.submit(lambda: make_project_df(project_id, headers)).result()

    print(milestones_url_encode)

    milestone_df = tpool.submit(lambda: make_milestone_df(project_id, milestones_url_encode, headers)).result()
    holiday_df = tpool.submit(make_holiday_df).result()
    project_holiday_df = pd.DataFrame(
        data=project_holidays, columns=['project_holidays'])
    man_powers_df = create_man_powers_df(man_powers_adjustment)

    # マイルストーンの開始日、終了日を取得
    milestone_start_date = to_datetime(milestone_df.start_date.min())
    milestone_due_date = to_datetime(milestone_df.due_date.max())

    # issueデータを取得
    issue_df, maked_issue_df, closed_issue_df, total_issue_points = make_issue_df_with_maked_closed(
        maked_labels, ignore_iids,  milestone_start_date, tpool, project_id, milestones_url_encode, headers, maked_wegiht)

    # 日付のデータフレームを作成 (マイルストーン期間中)
    day_df = make_day_df(milestone_start_date, holiday_df,
                         project_holiday_df, is_working_holiday)

    # 計画ポイントを累積で計算
    day_plan_accum_df = plot_plan_accum_points(
        day_df, man_powers_df, milestone_start_date, milestone_due_date,
        total_issue_points, base_man_power_rate, buffer_rate, is_milestone_first)

    # ポイントプロット
    # -> 計画
    day_plan_df = plot_plan(day_plan_accum_df, total_issue_points)
    day_plan_slice_df = slice_graph_range(day_plan_df, disp_range_enlargement)
    # -> 完了
    day_plan_done_df = plot_done(
        day_plan_slice_df, maked_issue_df, closed_issue_df, total_issue_points, d_today)
    # -> 予測
    burndown_df = plot_prediction(
        day_plan_done_df, d_today, mean_range_days, prediction_one_day_points)

    # アドバイスを生成
    advice = make_advice(burndown_df, d_today, mean_range_days)

    # マイルストーンの期日とマンパワーから計算した期日が違う場合に、マイルストーンの期日をグラフに描く準備
    limit_days += make_limit_days(burndown_df, milestone_due_date)
    title=project_df['name'][0] + " - " + ' | '.join(milestone_df.sort_values('title')['title'].array)
    
    end_diff_days = calc_plan_prediction_end_diff_days(burndown_df, d_today)
    today_buffer_points = calc_today_buffer_points(burndown_df, d_today)

    # バーンダウンチャートの描画
    output = draw_burndown_chart(
        burndown_df, issue_df, title, advice, total_issue_points, d_today, limit_days, end_diff_days)

    # -----------------------------------------------------------------
    # slackへのアップロード
    datestamp = makeTimestampWith()
    comment = "{0} | {2} | バッファ = {1}人日 ".format(
        datestamp,
        round(today_buffer_points, 1),
        'good' if end_diff_days <= 0 else 'bad'
    )
    upload_burndown_chart_slack_channel(
        slack_token, slack_channel, output.getvalue(), comment)
    return {
        'isBase64Encoded': False,
        'statusCode': 200,
        'headers': {},
        'body': '{"message": "Success!. File Uploaded to Slack."}'
    }


def make_holiday_df():
    holiday_api = "https://holidays-jp.github.io/api/v1/date.csv"
    res = requests.get(holiday_api,)
    holiday_df = pd.read_csv(io.StringIO(res.text), names=('holiday', 'name'))
    return holiday_df


def make_project_df(project_id, headers):
    project_api = "https://gitlab.com/api/v4/projects/{project_id}"
    project_api = project_api.format(project_id=project_id)
    res = requests.get(project_api, headers=headers)
    j = json.loads(res.text)
    project_df = json_normalize(j)
    return project_df


def make_milestone_df(project_id, milestones, headers):
    array = []
    for milestone in milestones:
        milestone_api = "https://gitlab.com/api/v4/projects/{project_id}/milestones?title={milestone}"
        milestone_api = milestone_api.format(
            project_id=project_id,  milestone=milestone)
        res = requests.get(milestone_api, headers=headers)
        j = json.loads(res.text)
        milestone_df = json_normalize(j)
        array.append(milestone_df)
    return pd.concat(array, axis=0)
  
def make_issue_df(project_id, milestones, headers):
    array = []
    for milestone in milestones:
        issue_api = "https://gitlab.com/api/v4/projects/{project_id}/issues?scope=all&per_page=1000&milestone={milestone}"
        issue_api = issue_api.format(project_id=project_id,  milestone=milestone)
        res = requests.get(issue_api, headers=headers)
        j = json.loads(res.text)
        issue_raw_df = json_normalize(j)
        array.append(issue_raw_df)
    return pd.concat(array, axis=0)

def findMakedDate(project_id, issue_iid, headers, maked_labels):
    label_issue_api = "https://gitlab.com/api/v4/projects/{project_id}/issues/{issue_iid}/resource_label_events"
    label_issue_api = label_issue_api.format(
        project_id=project_id,  issue_iid=issue_iid)
    res = requests.get(label_issue_api, headers=headers)
    j = json.loads(res.text)
    if len(j) == 0:
        return None
    d = json_normalize(j)
    maked_date_df = d.loc[((d['action'] == 'add') & (d['label.name'].apply(
        lambda label:label in maked_labels))), ['created_at']].tail(1)
    maked_date = np.nan if (
        maked_date_df.empty) else maked_date_df['created_at'].values[0]
    return maked_date


def zeroSeriesToNan(pram_df, colName):
    old_row = None
    for row in pram_df.itertuples():
        value = getattr(row, colName)
        old_value = getattr(old_row, colName) if old_row != None else -1
        if(value == 0 and value == old_value):
            pram_df.loc[pram_df.index == row.Index, colName] = np.nan
        old_row = row
    return pram_df


def create_man_powers_df(man_powers_adjustment):
    man_powers_array = []
    for obj in man_powers_adjustment:
        for key, value in obj.items():
            man_powers_array.append(
                [datetime.strptime(key, '%Y-%m-%d'), value])
    man_powers_df = pd.DataFrame(data=man_powers_array, columns=[
        'day', 'adjustment_point'])

    return man_powers_df.set_index('day')


def make_issue_df_with_maked_closed(maked_labels, ignore_iids, milestone_start_date, tpool, project_id, milestones, headers, maked_wegiht):
    # スレッド処理により並列にアクセス
    issue_raw_df = tpool.submit(lambda: make_issue_df(
        project_id, milestones, headers)).result()
    issue_raw_df['maked_at_submit'] = issue_raw_df['iid'].apply(
        lambda iid: tpool.submit(
            lambda: findMakedDate(project_id, iid, headers, maked_labels)))
    issue_raw_df['maked_at'] = issue_raw_df['maked_at_submit'].apply(
        lambda f: f.result())

    # 除外設定があれば、issueを除外する
    if len(ignore_iids) > 0:
        issue_raw_df = issue_raw_df[issue_ｒaw_df['iid'].apply(
            lambda iid: not iid in ignore_iids)]
        issue_raw_df = issue_raw_df.reset_index()

    # all
    issue_df = issue_raw_df[['title', 'state', 'created_at', 'maked_at',
                             'closed_at', 'labels', 'time_stats.time_estimate']].copy()
    issue_df['issue_points'] = issue_df['time_stats.time_estimate'].apply(
        lambda x: x/3600/8)

    # makedラベルの付与 (makedを示すラベルがついている場合は、close済みのものを除いて、statusをmakedに変更する)
    issue_df.loc[((issue_df['labels'].apply(lambda issue_labels: any(
        elem in maked_labels for elem in issue_labels))) & ~(issue_df['state'] == 'closed')), 'state']='maked'
    # makedポイントとclosedポイントの振り分け (closedしている場合は、closedのポイントとmakedのポイントをMAKED_WEGIHTの重みにより割り振る)
    issue_df['maked_points'] = issue_df.loc[issue_df['maked_at'].isnull(
    ) == False, 'issue_points'] * maked_wegiht
    issue_df['maked_points'] = issue_df['maked_points'].fillna(0)

    issue_df['closed_points'] = issue_df['issue_points'] - \
        issue_df['maked_points'].fillna(0)
    issue_df['closed_points'] = issue_df['closed_points'].fillna(0)

    # maked   作成済み日をdayキーに
    maked_issue_df = issue_df[['maked_at', 'maked_points']].dropna(subset=[
        'maked_at']).copy()
    maked_issue_df['day'] = maked_issue_df['maked_at'].apply(to_datetime)

    # closed  完了済み日をdayキーに
    closed_issue_df = issue_df[['closed_at', 'closed_points']].dropna(subset=[
        'closed_at']).copy()
    closed_issue_df['day'] = closed_issue_df['closed_at'].apply(to_datetime)

    # マイルストーンの開始日前に、makedやclosedになったものは、開始日にdoneしたものとする。
    maked_issue_df.loc[maked_issue_df['day'] <
                       milestone_start_date, 'day'] = milestone_start_date
    closed_issue_df.loc[closed_issue_df['day'] <
                        milestone_start_date, 'day'] = milestone_start_date

    # issueの合計ポイントを算出
    total_issue_points = issue_df['time_stats.time_estimate'].sum()/3600/8
    return (issue_df, maked_issue_df, closed_issue_df, total_issue_points)


def make_day_df(milestone_start_date, holiday_df, project_holidays_df, is_working_holiday):
    # マイルストーン開始日の20日前を開始日とする
    start = milestone_start_date - timedelta(days=15)
    # とりあえず100日後を終了日とする。
    end = start + timedelta(days=DEFAULT_DAYS)

    # マイルストーンの期間をもとに、X軸の日付を生成
    # 土日を除外
    s = 'B' if not is_working_holiday else 'D'
    df = pd.DataFrame(pd.date_range(
        start=start, end=end, freq=s), columns=["day"])
    if not is_working_holiday:
        # 祝日を除外
        df = df[df['day'].apply(lambda x: not (
            holiday_df['holiday'] == x.strftime('%Y-%m-%d')).sum() > 0)]
        # 有給休暇を除外
        df = df[df['day'].apply(lambda x: not (
            project_holidays_df['project_holidays'] == x.strftime('%Y-%m-%d')).sum() > 0)]

    idx = df[df['day'] < milestone_start_date].index[-1]
    return df[df.index >= idx].set_index('day')


def plot_plan_accum_points(df, man_powers_df, start_date, due_date, total_issue_points, base_man_power_rate, buffer_rate, is_milestone_first):
    # パターン1  = マイルストーンの終了日入力あり   マイルストーン優先 or マンパワー入力なし   => 終了日をぴったりにおわる。
    # パターン2  = マイルストーンの終了日入力なし   マンパワー入力あり　=> 終了日をぴったりにおわる。or 超えるパターンあり
    # パターン3  = マイルストーンの終了日入力なし   マンパワー入力なし   =>  エラー
    # -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    if start_date == None:
        raise Exception('マイルストーンの開始日を設定してください')

    exists_due_date = not due_date == None
    exists_base_man_power_rate = not base_man_power_rate == None
    def rate_func(): return base_man_power_rate * (1 - buffer_rate)

    # パターン１
    if exists_due_date and (is_milestone_first or base_man_power_rate == None):
        days = len(
            df[df.index.to_frame()['day'].between(start_date,  due_date)])
        # 一時的に人が少ない場合は多い場合に、母数を調整して一日あたりのベース作業量を見積もる
        ad = man_powers_df.sum()['adjustment_point'] * -1
        base_one_day_points = (total_issue_points + ad) / days
    # パターン２
    elif exists_base_man_power_rate:
        base_one_day_points = rate_func()
    # パターン３
    else:
        raise Exception('マイルストーンの期日か、アサインできる人員を入力してください')

    df['base_one_day_points'] = 0
    df.loc[df.index >= start_date,  'base_one_day_points'] = base_one_day_points
    df = pd.concat([df, man_powers_df], axis=1, join='outer')
    df['plan_one_day_points'] = df['base_one_day_points'] + \
        df['adjustment_point'].fillna(0)
    df['plan_accum_points'] = df['plan_one_day_points'].cumsum()
    return df


def plot_plan(df, total_issue_points):
    plan_end_df = df[df['plan_accum_points'] >= total_issue_points]
    if len(plan_end_df) == 0:
        raise Exception('計画がマイルストーンの範囲内に収まっていません。マイルストーンの開始日、期日を見直してください。')

    end = plan_end_df.index[0]
    df['plan'] = df.loc[:end, 'plan_accum_points'].apply(
        lambda x: total_issue_points - x)
    # 残ポイントの微妙な端数は0「完了」とみなします。
    df.loc[df['plan'] < 0.1, 'plan'] = 0
    df = zeroSeriesToNan(df, 'plan')
    return df[['plan', 'adjustment_point', 'plan_one_day_points']]


def slice_graph_range(df, disp_range_enlargement):
    plan_days = len(df[df['plan'].isnull() == False].index[1:])
    range_days = math.ceil(plan_days * disp_range_enlargement)
    index_end_date = df.index.date[range_days]
    return df[:index_end_date]


def plot_done(df, maked_issue_df, closed_issue_df, total_issue_points, d_today):
    def plot(issue_df, key): return pd.concat(
        [df, issue_df.groupby('day').sum()[key]], axis=1)
    df = plot(maked_issue_df, 'maked_points')
    df = plot(closed_issue_df, 'closed_points')

    df['done_points'] = df['maked_points'].fillna(
        0) + df['closed_points'].fillna(0)
    df.loc[df.index > d_today, 'done_points'] = np.nan
    df["done"] = total_issue_points - df['done_points'].cumsum()
    df.iloc[0]['done_points'] = np.nan
    return df[['plan', 'done', 'done_points', 'adjustment_point', 'plan_one_day_points', 'maked_points', 'closed_points']]


def plot_prediction(df, d_today, mean_range_days, prediction_one_day_points):
    # 直近過去数日間の平均消化ポイントの算出
    mean_points = df[df.index <= d_today].tail(
        mean_range_days)['done_points'].mean()
    print('実績 1日', mean_points)

    # 予測完了ポイントが指定ある場合は、それで予測線を描く
    if not prediction_one_day_points == None:
        mean_points = prediction_one_day_points

    # 直近　10日間の平均消化ポイントをプロット
    df.loc[df.index > d_today, 'mean_points'] = mean_points

    # 実績と予測線を合わせるために、０埋めする
    index = df.loc[df.index <= d_today, 'mean_points'].index[-1]
    df.loc[index:index, 'mean_points'] = 0
    df.loc[index:index, 'adjustment_point'] = 0

    # マンパワーの微調整を考慮した予測線にする。
    df['mean_aj_points'] = df['mean_points'] + df['adjustment_point'].fillna(0)

    # 平均消化ポイントから累積残ポイントをプロット
    done_cursor = df[df['done'].isnull() == False].tail(1)['done'].values[0]
    df['prediction'] = done_cursor - df['mean_aj_points'].cumsum()

    # 実績はきりの良い数字になるが、予測戦は小数点以下になることが多い、そこで、0.2未満は最後の日に含めるものとする。
    d = df.loc[(df['prediction'] < 0.2), 'prediction'].max()
    df.loc[(df['prediction'] == d), 'prediction'] = 0
    df.loc[(df['prediction'] < 0), 'prediction'] = np.nan
    return df[['plan', 'done', 'prediction', 'done_points', 'maked_points', 'closed_points', 'plan_one_day_points']]


def make_advice(df, d_today, mean_range_days):
    rest_days = calc_rest_days(df, d_today)
    mean_df = calc_mean_points(df, d_today, mean_range_days)
    done_mean_points = mean_df['done_points']
    plan_mean_points = mean_df['plan_one_day_points']
    today_buffer_points = calc_today_buffer_points(df, d_today)

    is_today_good = (df['plan'] - df['done']).dropna().tail(1).values[0] > 0
    summary = textwrap.dedent('''
    計画(平均)：1日={plan_mean_points:.1f}人日    バッファ(現在)：{today_buffer_points}人日
    実績(平均)：1日={done_mean_points:.1f}人日    残り日数  ：{rest_days}日
    ''').format(
        plan_mean_points=round(plan_mean_points, 1),
        done_mean_points=round(done_mean_points, 1),
        today_buffer_points=round(today_buffer_points, 1),
        rest_days=rest_days
    ).strip()

    # 計測不能
    is_boundless = len(df[df['prediction'] <= 0]) == 0

    if is_boundless:
        summary += "\n"
        summary += '★計画を見直してください!★'
    elif not is_today_good:
        advice_one_day_points = calc_advice_one_day_points(
            df, d_today, rest_days)
        cut_issue_points = calc_cut_issue_points(df)
        overtime_points = advice_one_day_points - done_mean_points
        overtime_hours = 8*(overtime_points)
        summary += "\n"
        summary += textwrap.dedent('''
      ★解決策★ ：　
       1.進捗を上げる (1日={done_mean_points:.1f}→{advice_one_day_points:.1f}人日/チーム残業={overtime_hours}h)
       2.スコープ調整 ({cut_issue_points} 人日分　カット)
       3.期日調整 (BurnDownChartsを参照)
      ''').format(
            done_mean_points=round(done_mean_points, 1),
            advice_one_day_points=round(advice_one_day_points, 1),
            overtime_hours=round(overtime_hours, 1),
            cut_issue_points=round(cut_issue_points, 1),
        ).strip()
    print(summary)
    return summary


def calc_rest_days(df, d_today):
    return df.loc[(df.index >= d_today) & (df['plan'] >= 0)].count()['plan']


def calc_today_buffer_points(df, d_today):
    today = df[df.index <= d_today].tail(1)
    return (today['plan'] - today['done']).values[0]


def calc_advice_one_day_points(df,  d_today, rest_days):
    rest_issue_points = df.loc[df.index <= d_today].tail(1)['done'].values[0]
    return rest_issue_points / rest_days


def calc_mean_points(df, d_today,  mean_range_days):
    # 先頭は範囲外のため除外します。
    df = df[1:]
    dfr = df[df.index <= d_today][['plan_one_day_points',
                                   'done_points']].dropna().tail(mean_range_days)
    if len(dfr) == 0:
        dfr = df[0:]
    return dfr.mean()


def calc_cut_issue_points(df):
    return df.loc[df.index == df['plan'].dropna().tail(1).index[0], 'prediction'].values[0]


def make_limit_days(df, due_date):
    plan_end_date = df.plan.dropna().index[-1]
    is_not_match_due_date = not plan_end_date == due_date
    return [{'date': due_date, 'label': '期日', 'color': 'gold'}] if is_not_match_due_date else []


def draw_burndown_chart(df, issue_df, title, advice, total_issue_points, d_today, limit_days, end_diff_days):
    # フォント
    plt.rcParams["font.size"] = 10
    plt.style.use('default')
    plt.rcParams['font.family'] = ['IPAexGothic']
    plt.rcParams['font.sans-serif'] = ['SimHei']

    #  1,2: 1行２列，　width_ration3:1
    fig, axs = plt.subplots(1, 2, figsize=(25, 6))
    gs = gridspec.GridSpec(1, 2, width_ratios=[5, 2])
    df = df.reset_index()
    # バーンダウンチャートを描画
    ax1 = plt.subplot(gs[0])
    draw_burndown_line(df, ax1, title, total_issue_points, d_today)
    draw_burndown_bar(df, ax1)
    # 期日のラベルをプロット
    draw_plan_end_label(df, len(df), total_issue_points)
    draw_prediction_end_day_label(
        df, len(df), total_issue_points, end_diff_days)
    # アドバイスを挿入
    draw_advice(advice, ax1, len(df), total_issue_points)
    # 日付の注釈を描画

    def search_idx(obj):
        date_index = df[df.day == to_datetime(obj['date'])].index
        if(len(date_index) > 0):
            return date_index[0]
        else:
            return -1

    limit_days = sorted(list(
        map(lambda obj: {**obj, 'idx': search_idx(obj)}, limit_days)), key=lambda x: x['idx'])
    f = draw_vline_day(df, ax1, len(limit_days))
    for obj in limit_days:
        f(**obj)

    # issue別棒グラフ
    ax2 = plt.subplot(gs[1])
    draw_issue_by_bar(issue_df, ax2)
    fig.tight_layout()
    # グラフを画像としてメモリ上に保存
    output = io.BytesIO()
    plt.savefig(output, format='png')
    return output


def draw_burndown_line(df, ax, title, total_issue_points, d_today):
    # 描画
    plan_color = 'b'
    done_color = 'r'
    prediction_color = 'g'
    df.plot(
        title=title,
        y=['plan', 'done', 'prediction'],
        grid=True,
        rot=0,
        figsize=(12, 6),
        style=[plan_color+'-', done_color+'.-', prediction_color+'--'],
        ax=ax)
    ax.legend(["計画", "実績", "予測"], fontsize=18)
    ax.xaxis.set_label_text('日数')
    ax.yaxis.set_label_text('人日')


def draw_burndown_bar(df, ax):
    df = df.rename(columns={'closed_points': 'closed', 'maked_points': 'maked'})[
        ['done_points', 'maked', 'closed']]
    df.plot(
        kind='bar',
        y=['closed', 'maked'],
        label=['closed', 'maked'],
        color=['#2da02c', '#ff7f0e'],
        stacked=True,
        grid=True,
        rot=45,
        width=0.7,
        figsize=(12, 6),
        ax=ax)

    # bar done した数のラベルをプロット
    df[df['done_points'] > 0].apply(lambda row: plt.text(row.name, row.done_points, round(
        row.done_points, 1), ha='center', va='bottom', fontsize=9), axis=1)


def calc_plan_prediction_end_diff_days(df, d_today):
    df = df[df.index >= d_today]
    plan_end_days = len(df['plan'].dropna().index)
    prediction_end_days = len(df['prediction'].dropna().index)
    return prediction_end_days - plan_end_days


def draw_plan_end_label(df, width, height):
    plan_df = df.loc[df['plan'] == 0,  'day'].head(1)
    if len(plan_df) > 0:
        x = plan_df.index[0]
        label = pd.to_datetime(plan_df.values[0]).strftime('%m/%d')
        draw_day_label(x+(width/50), (height/35), label, '#ADD8E6')


def draw_prediction_end_day_label(df, width, height, end_diff_days):
    prediction_df = df.loc[df['prediction'] == 0,  'day'].head(1)
    if len(prediction_df) > 0:
        x = prediction_df.index[0]
        day = prediction_df.values[0]
        mark = '+' if end_diff_days > 0 else ''
        label = pd.to_datetime(day).strftime(
            '%m/%d') + "(" + mark + str(end_diff_days) + "日)"
        draw_day_label(x+(width/50), (height/9 * -1), label, '#90EE90')


def draw_day_label(x, y, text, color):
    plt.text(x, y, text, ha='left', va='bottom', fontsize=12,
             backgroundcolor=color, color='black', fontweight='bold')


def draw_advice(advice, ax, width, height):
    textArea = TextArea(advice, multilinebaseline=False,
                        textprops=dict(color="crimson"))
    is_long_sentence = '★解決策★' in advice
    box = AnnotationBbox(
        textArea, (width/2 + (width*0.01), height - (height/40 * (3 if is_long_sentence else 1))))
    ax.add_artist(box)


def draw_vline_day(df, ax, length):
    length = max(length, 10)
    i = 1
    y_max = df['plan'].max()
    label_height = y_max*0.8
    tick = (label_height)/length

    def draw(idx, date, label, color, linestyle='--'):
        nonlocal i
        dt = to_datetime(date)
        x = idx
        label = pd.to_datetime(dt).strftime('%m/%d') + "[" + label + "]"
        ax.vlines(x, 0, y_max, linestyle=linestyle, linewidth=2, color=color)
        ax.annotate(
            label,
            backgroundcolor=color,
            color='black',
            xy=(x, y_max),
            xytext=(x+0.5, (label_height - (tick * i))))
        i += 1
    return draw


def draw_issue_by_bar(df, ax):
    # ピボットテーブル
    pt = pd.pivot_table(
        df,
        # 集計したい縦のキー
        index=['issue_points'],
        # 集計したい横のキー(複数指定化)
        columns='state',
        # 集計したい項目 (指定がなければ、上記のキーになっていない項目)
        values='title',
        # 個数をカウントする。これがないとValuesの平均値になる。
        aggfunc=lambda x: len(x),
        # # NaN を 0埋めする
        fill_value=0)

    if not 'closed' in pt.columns:
        pt['closed'] = 0
    if not 'maked' in pt.columns:
        pt['maked'] = 0
    if not 'opened' in pt.columns:
        pt['opened'] = 0

    pt.plot(
        kind='bar',
        title="タスク 工数別・完了件数",
        y=['closed', 'maked', 'opened'],
        label=['closed', 'maked', 'opened'],
        color=['#2da02c', '#ff7f0e', '#2077b4'],
        stacked=True,
        grid=True,
        rot=45,
        width=0.7,
        ax=ax)
    pt.reset_index().apply(draw_issue_count_label, axis=1)
    ax.xaxis.set_label_text('タスク (人日)')
    ax.yaxis.set_label_text('タスク (件数)')
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))


def draw_issue_count_label(obj):
    def plot(x, y, label): return plt.text(x, y, label, ha='center',
                                           va='center', fontsize=10, color='white', fontweight='extra bold')
    if obj.closed > 0:
        plot(obj.name, (obj.closed/2), int(obj.closed))
    if obj.maked > 0:
        plot(obj.name, (obj.closed + (obj.maked/2)), int(obj.maked))
    if obj.opened > 0:
        plot(obj.name, (obj.closed + obj.maked + (obj.opened/2)), int(obj.opened))


def upload_burndown_chart_slack_channel(token, channel, file_buf, comment):
    files = {'file': file_buf}
    param = {
        'token': token,
        'channels': channel,
        'filename': 'BurndownChart.png',
        'filetype': 'png',
        'initial_comment': comment,
        'title': 'BurndownChart'
    }
    r = requests.post(url="https://slack.com/api/files.upload",
                      params=param, files=files)
    print(r.text)


def makeTimestampWith():
    utc_now = datetime.now(timezone('UTC'))
    jst_now = utc_now.astimezone(timezone('Asia/Tokyo'))
    jpDayOfWeek = ['日', '月', '火', '水', '木',
                   '金', '土', ][int(jst_now.strftime("%w"))]
    return jst_now.strftime("%m/%d({0}) %H:%M ").format(jpDayOfWeek)


def to_datetime(ymd):
    return datetime.strptime(str(ymd)[:10], '%Y-%m-%d')


def to_datetime_from_object(date):
    return datetime.strptime(date.to_string().lstrip('0    '),  '%Y-%m-%d')
